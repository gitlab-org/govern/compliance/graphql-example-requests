### Get compliance violations

[`get_compliance_violations.graphql`](get_compliance_violations.graphql) query is used to
get the list of merge request compliance violations for a group.

Response

```json
{
  "data": {
    "group": {
      "mergeRequestViolations": {
        "nodes": [
          {
            "id": "gid://gitlab/MergeRequests::ComplianceViolation/2634850",
            "severityLevel": "HIGH",
            "reason": "APPROVED_BY_INSUFFICIENT_USERS",
            "violatingUser": {
              "id": "gid://gitlab/User/411701",
              "name": "Kushal Pandya",
              "username": "kushalpandya",
              "avatarUrl": "/uploads/-/system/user/avatar/411701/avatar.png",
              "webUrl": "https://gitlab.com/kushalpandya"
            },
            "mergeRequest": {
              "id": "gid://gitlab/MergeRequest/190400703",
              "title": "Fix typos in README",
              "mergedAt": "2022-11-23T07:00:48Z",
              "webUrl": "https://gitlab.com/issue-reproduce/gitlab-okrs/-/merge_requests/1",
              "author": {
                "id": "gid://gitlab/User/411701",
                "name": "Kushal Pandya",
                "username": "kushalpandya",
                "avatarUrl": "/uploads/-/system/user/avatar/411701/avatar.png",
                "webUrl": "https://gitlab.com/kushalpandya"
              },
              "mergeUser": {
                "id": "gid://gitlab/User/411701",
                "name": "Kushal Pandya",
                "username": "kushalpandya",
                "avatarUrl": "/uploads/-/system/user/avatar/411701/avatar.png",
                "webUrl": "https://gitlab.com/kushalpandya"
              },
              "committers": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/13751098",
                    "name": "Kushal Pandya",
                    "username": "kushalspandya",
                    "avatarUrl": "/uploads/-/system/user/avatar/13751098/avatar.png",
                    "webUrl": "https://gitlab.com/kushalspandya"
                  }
                ]
              },
              "participants": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/411701",
                    "name": "Kushal Pandya",
                    "username": "kushalpandya",
                    "avatarUrl": "/uploads/-/system/user/avatar/411701/avatar.png",
                    "webUrl": "https://gitlab.com/kushalpandya"
                  }
                ]
              },
              "approvedBy": {
                "nodes": []
              },
              "ref": "!1",
              "fullRef": "issue-reproduce/gitlab-okrs!1",
              "sourceBranch": "kushalpandya-main-patch-23271",
              "sourceBranchExists": false,
              "targetBranch": "main",
              "targetBranchExists": true,
              "project": {
                "id": "gid://gitlab/Project/41292308",
                "avatarUrl": null,
                "name": "GitLab OKRs",
                "webUrl": "https://gitlab.com/issue-reproduce/gitlab-okrs",
                "complianceFrameworks": {
                  "nodes": []
                }
              }
            }
          },
          {
            "id": "gid://gitlab/MergeRequests::ComplianceViolation/3",
            "severityLevel": "INFO",
            "reason": "APPROVED_BY_INSUFFICIENT_USERS",
            "violatingUser": {
              "id": "gid://gitlab/User/5209897",
              "name": "Robert Hunt",
              "username": "rob.hunt",
              "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
              "webUrl": "https://gitlab.com/rob.hunt"
            },
            "mergeRequest": {
              "id": "gid://gitlab/MergeRequest/137787173",
              "title": "Add new file",
              "mergedAt": "2022-02-01T09:15:05Z",
              "webUrl": "https://gitlab.com/issue-reproduce/compliance-tanuki/test-violations/-/merge_requests/1",
              "author": {
                "id": "gid://gitlab/User/5209897",
                "name": "Robert Hunt",
                "username": "rob.hunt",
                "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                "webUrl": "https://gitlab.com/rob.hunt"
              },
              "mergeUser": {
                "id": "gid://gitlab/User/5209897",
                "name": "Robert Hunt",
                "username": "rob.hunt",
                "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                "webUrl": "https://gitlab.com/rob.hunt"
              },
              "committers": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "participants": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "ref": "!1",
              "fullRef": "issue-reproduce/compliance-tanuki/test-violations!1",
              "sourceBranch": "test-violations",
              "sourceBranchExists": false,
              "targetBranch": "main",
              "targetBranchExists": true,
              "project": {
                "id": "gid://gitlab/Project/33279309",
                "avatarUrl": null,
                "name": "Test Violations",
                "webUrl": "https://gitlab.com/issue-reproduce/compliance-tanuki/test-violations",
                "complianceFrameworks": {
                  "nodes": []
                }
              }
            }
          },
          {
            "id": "gid://gitlab/MergeRequests::ComplianceViolation/2",
            "severityLevel": "INFO",
            "reason": "APPROVED_BY_COMMITTER",
            "violatingUser": {
              "id": "gid://gitlab/User/5209897",
              "name": "Robert Hunt",
              "username": "rob.hunt",
              "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
              "webUrl": "https://gitlab.com/rob.hunt"
            },
            "mergeRequest": {
              "id": "gid://gitlab/MergeRequest/137787173",
              "title": "Add new file",
              "mergedAt": "2022-02-01T09:15:05Z",
              "webUrl": "https://gitlab.com/issue-reproduce/compliance-tanuki/test-violations/-/merge_requests/1",
              "author": {
                "id": "gid://gitlab/User/5209897",
                "name": "Robert Hunt",
                "username": "rob.hunt",
                "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                "webUrl": "https://gitlab.com/rob.hunt"
              },
              "mergeUser": {
                "id": "gid://gitlab/User/5209897",
                "name": "Robert Hunt",
                "username": "rob.hunt",
                "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                "webUrl": "https://gitlab.com/rob.hunt"
              },
              "committers": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "participants": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "ref": "!1",
              "fullRef": "issue-reproduce/compliance-tanuki/test-violations!1",
              "sourceBranch": "test-violations",
              "sourceBranchExists": false,
              "targetBranch": "main",
              "targetBranchExists": true,
              "project": {
                "id": "gid://gitlab/Project/33279309",
                "avatarUrl": null,
                "name": "Test Violations",
                "webUrl": "https://gitlab.com/issue-reproduce/compliance-tanuki/test-violations",
                "complianceFrameworks": {
                  "nodes": []
                }
              }
            }
          },
          {
            "id": "gid://gitlab/MergeRequests::ComplianceViolation/1",
            "severityLevel": "INFO",
            "reason": "APPROVED_BY_MERGE_REQUEST_AUTHOR",
            "violatingUser": {
              "id": "gid://gitlab/User/5209897",
              "name": "Robert Hunt",
              "username": "rob.hunt",
              "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
              "webUrl": "https://gitlab.com/rob.hunt"
            },
            "mergeRequest": {
              "id": "gid://gitlab/MergeRequest/137787173",
              "title": "Add new file",
              "mergedAt": "2022-02-01T09:15:05Z",
              "webUrl": "https://gitlab.com/issue-reproduce/compliance-tanuki/test-violations/-/merge_requests/1",
              "author": {
                "id": "gid://gitlab/User/5209897",
                "name": "Robert Hunt",
                "username": "rob.hunt",
                "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                "webUrl": "https://gitlab.com/rob.hunt"
              },
              "mergeUser": {
                "id": "gid://gitlab/User/5209897",
                "name": "Robert Hunt",
                "username": "rob.hunt",
                "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                "webUrl": "https://gitlab.com/rob.hunt"
              },
              "committers": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "participants": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/5209897",
                    "name": "Robert Hunt",
                    "username": "rob.hunt",
                    "avatarUrl": "/uploads/-/system/user/avatar/5209897/avatar.png",
                    "webUrl": "https://gitlab.com/rob.hunt"
                  }
                ]
              },
              "ref": "!1",
              "fullRef": "issue-reproduce/compliance-tanuki/test-violations!1",
              "sourceBranch": "test-violations",
              "sourceBranchExists": false,
              "targetBranch": "main",
              "targetBranchExists": true,
              "project": {
                "id": "gid://gitlab/Project/33279309",
                "avatarUrl": null,
                "name": "Test Violations",
                "webUrl": "https://gitlab.com/issue-reproduce/compliance-tanuki/test-violations",
                "complianceFrameworks": {
                  "nodes": []
                }
              }
            }
          }
        ]
      }
    }
  }
}
```
