### Get compliance frameworks

[`get_compliance_frameworks.graphql`](get_compliance_frameworks.graphql) query is used to
get the list of compliance frameworks for a group.

Response:

```json
{
  "data": {
    "namespace": {
      "complianceFrameworks": {
        "nodes": [
          {
            "id": "gid://gitlab/ComplianceManagement::Framework/3",
            "name": "FIPS",
            "default": false,
            "description": "Federal Information Processing Standards",
            "color": "#000000",
            "pipelineConfigurationFullPath": null
          },
          {
            "id": "gid://gitlab/ComplianceManagement::Framework/7",
            "name": "FedRAMP",
            "default": false,
            "description": "Federal Risk and Authorization Management Program",
            "color": "#00008B",
            "pipelineConfigurationFullPath": null
          },
          {
            "id": "gid://gitlab/ComplianceManagement::Framework/2",
            "name": "GDPR",
            "default": true,
            "description": "General Data Protection Regulation",
            "color": "#ff0000",
            "pipelineConfigurationFullPath": null
          },
          {
            "id": "gid://gitlab/ComplianceManagement::Framework/1",
            "name": "HIPAA",
            "default": false,
            "description": "Health Insurance Portability and Accountability Act",
            "color": "#009966",
            "pipelineConfigurationFullPath": null
          },
          {
            "id": "gid://gitlab/ComplianceManagement::Framework/8",
            "name": "PCI DSS",
            "default": false,
            "description": "Payment Card Industry Data Security Standard",
            "color": "#87CEEB",
            "pipelineConfigurationFullPath": null
          },
          {
            "id": "gid://gitlab/ComplianceManagement::Framework/5",
            "name": "SOX",
            "default": false,
            "description": "Sarbanes-Oxley Act",
            "color": "#A020F0",
            "pipelineConfigurationFullPath": null
          }
        ]
      }
    }
  }
}
```
