// https://docs.gitlab.com/ee/api/status_checks.html#set-status-of-an-external-status-check

const GITLAB_URL = 'https://gitlab.com';
const GITLAB_PAT = 'todoYourPATHere';
const PROJECT_ID = '1234';
const MERGE_REQUEST_IID = '5678';

const SHA = 'dingfooasd'; // SHA at HEAD of the source branch
const EXTERNAL_STATUS_CHECK_ID = '9876'; // ID of an external status check
const STATUS = 'failed'; // 'passed' | 'failed'

fetch(`${GITLAB_URL}/api/v4/projects/${PROJECT_ID}/merge_requests/${MERGE_REQUEST_IID}/status_check_responses`, {
  "headers": {
    "accept": "application/json",
    "content-type": "application/json",
    "PRIVATE-TOKEN": `${GITLAB_PAT}`,
  },
  "body": JSON.stringify({
    sha: SHA,
    external_status_check_id: EXTERNAL_STATUS_CHECK_ID,
    status: STATUS
  }),
  "method": "POST",
  "mode": "cors",
  "credentials": "include"
});