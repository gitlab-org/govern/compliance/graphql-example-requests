### Create a compliance framework

[`create_compliance_framework.graphql`](create_compliance_framework.graphql) query is used to create a compliance 
framework for the group.

Response:

```json
{
  "data": {
    "createComplianceFramework": {
      "framework": {
        "id": "gid://gitlab/ComplianceManagement::Framework/10",
        "name": "PCI DSS",
        "default": true,
        "description": "Payment Card Industry Data Security Standard",
        "color": "#87CEEB",
        "pipelineConfigurationFullPath": null
      },
      "errors": []
    }
  }
}
```

### Update a compliance framework

[`update_compliance_framework.graphql`](update_compliance_framework.graphql) query is used to update an existing
compliance framework for the group.

Response:

```json
{
  "data": {
    "updateComplianceFramework": {
      "complianceFramework": {
        "id": "gid://gitlab/ComplianceManagement::Framework/10",
        "name": "PCI DSS",
        "default": true,
        "description": "Payment Card Industry Data Security Standard",
        "color": "#FF0000",
        "pipelineConfigurationFullPath": null
      }
    }
  }
}
```

### Destroy a compliance framework

[`destroy_compliance_framework.graphql`](destroy_compliance_framework.graphql) query is used to delete a compliance
framework for the group.

Response:

```json
{
  "data": {
    "destroyComplianceFramework": {
      "errors": []
    }
  }
}
```
