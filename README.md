# GraphQL example requests
This project contains a collection of frequently used GraphQL queries and mutations related to the
[compliance group](https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/).

## How to run the example requests
Visit the [GraphiQL explorer](https://gitlab.com/-/graphql-explorer) on GitLab.com and paste the example requests in
the playground.

## Queries

1. [Get compliance frameworks](queries/compliance_frameworks/README.md#get-compliance-frameworks)
2. [Get compliance violations](queries/merge_requests/compliance_violations/README.md#get-compliance-violations)

## Mutations

1. [Create compliance framework](mutations/compliance_frameworks/README.md#create-a-compliance-framework)
2. [Update compliance framework](mutations/compliance_frameworks/README.md#update-a-compliance-framework)
3. [Destroy compliance framework](mutations/compliance_frameworks/README.md#destroy-a-compliance-framework)